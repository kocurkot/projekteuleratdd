﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ProjektEuler
{
    [TestFixture]
    class ProjektEuleraTest
    {
        ProjektEulera projektEulera;
        [SetUp]
        public void setUp()
        {
            projektEulera = new ProjektEulera();
        }

        [Test]        
        public void takeLast10DigitsTest()
        {
            //Arange
            String actual, expected;
            expected = "4351866187";
            int numberOfDigits = 10;
            BigInteger number = BigInteger.Parse("1894651564895484894864351866187");           
            //Act
            actual = projektEulera.takeLastDigits(number, numberOfDigits);
            //Assert
            Assert.AreEqual(expected,actual);
        }

        [Test]
        public void takeLast0DigitsTest()
        {
            //Arange
            String actual, expected;
            expected = "";
            int numberOfDigits = 0;
            BigInteger number = BigInteger.Parse("1894651564895484894864351866187");           
            //Act
            actual = projektEulera.takeLastDigits(number, numberOfDigits);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void solutionFor10DigitsTest()
        {
            //Arrange
            int digits = 10;
            String actual, expected;
            expected = "9110846700";            
            //Act
            actual = projektEulera.solution(digits);
            //Assert
            Assert.AreEqual(expected, actual);
        }

    }
}
