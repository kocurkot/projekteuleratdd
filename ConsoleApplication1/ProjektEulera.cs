﻿using System;
using System.Collections.Generic;
using System.Linq;
//Task Project Euler 48
//https://projecteuler.net/problem=48
//Used C# Coding Conventions
//Wojciech kocik 13K3

using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ProjektEuler
{
    /// <summary>
    /// Solved Project Euler 48.
    /// </summary>
    class ProjektEulera
    {
        /// <summary>
        /// Solved Project Euler 48.
        /// </summary>
        /// <param name="numberOfDigits">Specify, number of shown digits.</param>
        /// <param name="number">numer what we want to take last digits</param>
        public String takeLastDigits(BigInteger number, int numberOfDigits)
        {
            if (numberOfDigits == 0){
                return "";
            }
            String numberString = number.ToString();
            return numberString.Substring(numberString.Length - numberOfDigits, numberOfDigits);
        }

        /// <summary>
        /// Solution number 1.
        /// </summary>
        /// <param name="numberOfDigits">Specify, number of shown digits.</param>
        public string solution(int numberOfDigits)
        {
            BigInteger sum = 0, _tempSum;
            int maxPower = 1000;
            for (int i = 1; i <= maxPower; i++)
            {
                _tempSum = BigInteger.Pow(i, i);
                sum += _tempSum;
            }

            return takeLastDigits(sum, numberOfDigits);
        }
    }
}
